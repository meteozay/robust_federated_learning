# Robust Federated Learning

Code for the experiments in the ICLR 2020 submission "Robust Federated Learning Through Representation Matching and Adaptive Hyper-parameters". The main python executable is `main.py`. Results are saved under a `./runs/` directory created at the invocation directory. An invocation of `main.py` will save various accuracy metrics as well as the model parameters in the file `./runs/{task name}_{job idx}`. Accuracy figures as well as several diagnostics are also printed out. 

For the MNIST and CIFAR10 tasks, the datasets are downloaded automatically under the `data` folder in the working directory. For the keyword spotting task, the speech commands dataset needs to be downloaded manually from [here](http://download.tensorflow.org/data/speech_commands_v0.01.tar.gz) and the path provided using the option --speech-commands-dataset. For MNIST and CIFAR10, an initial time-consuming pre-processing step is needed to split the dataset into 10 parts, where each part has examples belonging to only one class. To accelerate runtime, the dataset splitting indices are cached on disk in the file `processed_datasets_cache` on the first run and reused in subsequent runs. 

### General usage
```shell
main.py [-h] [--epochs EPOCHS] [--train-batch-size TRAIN_BATCH_SIZE]
               [--val-batch-size VAL_BATCH_SIZE] [--task {mnist,cifar10,KWS}]
               [--baseline-cutoff-interval BASELINE_CUTOFF_INTERVAL]
               [--adaptive-hparams] [--RM-mode] [--RM-coeff RM_COEFF]
               [--hyper-lr HYPER_LR]
               [--weight-divergence-coeff WEIGHT_DIVERGENCE_COEFF]
               [--n-val-iters-per-round N_VAL_ITERS_PER_ROUND]
               [--refresh-optimizers] [--shards-per-client SHARDS_PER_CLIENT]
               [--initial-precision INITIAL_PRECISION] [--iid-dataset]
               [--windowed-updates] [--no-reinforce-baseline]
               [--entropy-coeff ENTROPY_COEFF]
               [--entropy-threshold ENTROPY_THRESHOLD]
               [--n-clients-per-round N_CLIENTS_PER_ROUND]
               [--mnist-n-hidden MNIST_N_HIDDEN] [--momentum MOMENTUM]
               [--nesterov NESTEROV] [--print-freq PRINT_FREQ]
               [--job-idx JOB_IDX] [--schedule-file SCHEDULE_FILE]
               [--speech-commands-dataset SPEECH_COMMANDS_DATASET]
               [--use-cuda] [--num-loader-workers NUM_LOADER_WORKERS]
```
arguments:
```
  -h, --help            show this help message and exit
  --epochs EPOCHS       number of total epochs to run
  --train-batch-size TRAIN_BATCH_SIZE
                        mini-batch size for training
  --val-batch-size VAL_BATCH_SIZE
                        mini-batch size for validation
  --task {mnist,cifar10,KWS}
                        Choose between MNIST, CIFAR10 and keyword spotting
                        (KWS) task
  --baseline-cutoff-interval BASELINE_CUTOFF_INTERVAL
                        number of rounds to consider for the REINFORCE
                        baseline (Z in the paper)
  --adaptive-hparams    Use online adaptive hyperparameter optimization scheme
                        (default: False)
  --RM-mode             Use Representation Matching loss (default: False)
  --RM-coeff RM_COEFF   Representation matching loss coefficient
  --hyper-lr HYPER_LR   Hyper learning rate
  --weight-divergence-coeff WEIGHT_DIVERGENCE_COEFF
                        coefficient of the weight divergence loss
  --n-val-iters-per-round N_VAL_ITERS_PER_ROUND
                        number of validation mini-batches per client in each
                        round
  --refresh-optimizers  Reinitialize optimizers after every round (default:
                        False)
  --shards-per-client SHARDS_PER_CLIENT
                        number of shards with different labels per each client
  --initial-precision INITIAL_PRECISION
                        Initial precision for the hyper-parameters Gaussian
  --iid-dataset         Use iid dataset distribution (default: False)
  --windowed-updates    Update using all rewards/actions in the cutoff window
                        (default: False)
  --no-reinforce-baseline
                        Do not use a baseline in the online reinforce
                        algorithm (default: False)
  --entropy-coeff ENTROPY_COEFF
                        Coefficient of the entropy regularization term
  --entropy-threshold ENTROPY_THRESHOLD
                        Entropy threshold H_{min} used in the entropy
                        regularization term
  --n-clients-per-round N_CLIENTS_PER_ROUND
                        number of clients participating in each round
  --mnist-n-hidden MNIST_N_HIDDEN
                        number of neurons in each hidden layer of the MNIST
                        model
  --momentum MOMENTUM   momentum
  --nesterov NESTEROV   nesterov momentum
  --print-freq PRINT_FREQ, -p PRINT_FREQ
                        print frequency (default: 10)
  --job-idx JOB_IDX     job index provided by the job manager
  --schedule-file SCHEDULE_FILE
                        path to schedule file controlling the learning rate
                        and number of SGD iterations per round
  --speech-commands-dataset SPEECH_COMMANDS_DATASET
                        path to speech command dataset
  --use-cuda            Use GPUs (default: False)
  --num-loader-workers NUM_LOADER_WORKERS
                        number of workers loading data
```
### Performing the experiments in the paper
The three YAML files in the experiments folder : `mnist.yaml`, `cifar10.yaml`, and `KWS.yaml` each contains a list of the invocation commands needed to run the experiments described in the paper.

### Notes

- Code development and all experiments were done with Python 3.6 and pytorch 1.2.0. 
- All experiments were conducted on NVidia TitanXP GPUs.