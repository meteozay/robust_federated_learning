from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import argparse
import os
import shutil
import time
import subprocess
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import numpy as np
import yaml
import collections
from torch.autograd import Variable
import copy
import random
from torch.autograd import grad



from model import mnist_model,mnist_encoder,mnist_decoder,cifar10_model,cifar10_encoder,cifar10_decoder,speech_model,speech_encoder,speech_decoder
from datasets import single_class_dataset,multi_class_dataset,single_class_speech_dataset,audio_load_transform
from hparam_optimization import learnable_highd_gaussian,get_hyper_loss

parser = argparse.ArgumentParser(description='PyTorch Robust federated learning')
parser.add_argument('--epochs', default=100, type=int,
                    help='number of total epochs to run')

parser.add_argument('--train-batch-size', default=64, type=int,
                    help='mini-batch size for training ')
parser.add_argument('--val-batch-size', default=64, type=int,
                    help='mini-batch size for validation ')



parser.add_argument('--task', type=str, choices = ['mnist','cifar10','KWS'],default = 'mnist', help='Choose between MNIST, CIFAR10 and keyword spotting (KWS) task')


parser.add_argument('--baseline-cutoff-interval', default=5, type=int,
                    help='number of rounds to consider for the REINFORCE baseline (Z in the paper)  ')



parser.add_argument('--adaptive-hparams',  action='store_true',
                    help='Use online adaptive hyperparameter optimization scheme   (default: False)')

parser.add_argument('--RM-mode',  action='store_true',
                    help='Use Representation Matching loss  (default: False)')

parser.add_argument('--RM-coeff', default=0.0001, type=float, help='Representation matching loss coefficient')

parser.add_argument('--hyper-lr', default=0.01, type=float, help='Hyper learning rate')

parser.add_argument('--force-train-iters-per-round', default=-1, type=int, help='For benchmarking purposes. A positive number would force a fixed number of SGD training iterations per rounds ')


parser.add_argument('--weight-divergence-coeff', default=0.000, type=float, help='coefficient of the weight divergence loss')

parser.add_argument('--n-val-iters-per-round', default=1, type=int,
                    help='number of validation mini-batches per client in each round')

parser.add_argument('--refresh-optimizers',  action='store_true',
                    help='Reinitialize optimizers after every round   (default: False)')



parser.add_argument('--shards-per-client', default=1, type=int,
                    help='number of shards with different labels per each client ')


parser.add_argument('--initial-precision', default=60.0, type=float, help='Initial precision for the hyper-parameters Gaussian')                    


parser.add_argument('--iid-dataset',  action='store_true',
                    help='Use iid dataset distribution   (default: False)')

parser.add_argument('--windowed-updates',  action='store_true',
                    help='Update using all rewards/actions in the cutoff window  (default: False)')

parser.add_argument('--no-reinforce-baseline',  action='store_true',
                    help='Do not use a baseline in the online reinforce algorithm (default: False)')


parser.add_argument('--entropy-coeff', default=1.0, type=float, help='Coefficient of the entropy regularization term')
parser.add_argument('--entropy-threshold', default=2.0, type=float, help='Entropy threshold H_{min} used in the entropy regularization term')

parser.add_argument('--n-clients-per-round', default=10, type=int,
                    help='number of clients participating in each round')

parser.add_argument('--mnist-n-hidden', default=100, type=int,
                    help='number of neurons in each hidden layer of the MNIST model')

parser.add_argument('--momentum', default=0.9,type=float, help='momentum')
parser.add_argument('--nesterov', default=True, type=bool, help='nesterov momentum')


parser.add_argument('--print-freq', '-p', default=50, type=int,
                    help='print frequency (default: 10)')

parser.add_argument('--job-idx', default=0, type=int,
                    help='job index provided by the job manager')

parser.add_argument('--schedule-file', default='', type=str,
                    help='path to schedule file controlling the learning rate and number of SGD iterations per round ')

parser.add_argument('--speech-commands-dataset', default='', type=str,
                    help='path to speech command dataset ')

parser.add_argument('--use-cuda',  action='store_true',
                    help='Use GPUs   (default: False)')


parser.add_argument('--num-loader-workers', default=1, type=int,
                    help='number of workers loading data')


parser.set_defaults(augment=True)




def get_backward_taps(forward_taps,pool_pos,decoder,print_diagnostics = False):
    current_start = 0
    pool_pos_idx = len(pool_pos)-1
    backward_taps_ladder = []
    for tap,y in zip(decoder.taps,forward_taps[-1:0:-1]):
        x = y
        print('tap of size {} taken from forward pass'.format(x.size())) if print_diagnostics else 0
        for i in range(current_start,tap+1):
            print('stacking ', repr(decoder.stack[i])) if print_diagnostics else 0
            if isinstance(decoder.stack[i],nn.MaxUnpool2d):
               x = decoder.stack[i](x,pool_pos[pool_pos_idx])
               pool_pos_idx -= 1
            else:
               x = decoder.stack[i](x)
            current_start += 1
        backward_taps_ladder.append(x)
        print('putting tap of size {} in ladder'.format(x.size())) if print_diagnostics else 0
    return backward_taps_ladder

def train_rec(forward_model,backward_model,main_model,optimizer,loader,n_iterations = -1,weight_divergence_coeff = 0.0):
    criterion = nn.CrossEntropyLoss()
    forward_model.train()
    backward_model.train()    
    iter = 0
    total_rec_loss = 0.0
    if(weight_divergence_coeff > 0.0):
        print('using weight divergence coeff ', repr(weight_divergence_coeff))
    unique_labels = np.empty(0)
    for (x,y) in loader:
        unique_labels = np.unique(np.concatenate((y.numpy(),unique_labels)))
        x,y = x.to(device),y.to(device)
        iter += 1
        forward_acts,unpool_pos,output = forward_model(x)
        rec_loss = 0.0
        backward_acts = get_backward_taps(forward_acts,unpool_pos,backward_model) #backward_model(forward_acts[-1],unpool_pos)
        forward_acts_main,unpool_pos_main,_ = main_model(x)
        forward_acts_main = [x.detach() for x in forward_acts_main]
        forward_backward_pairs = zip(forward_acts_main[:-1],backward_acts[::-1])                

            
        rec_loss = sum([((x-y)**2).view(x.size(0),-1).sum(-1).mean() for x,y in forward_backward_pairs])
            
        total_rec_loss += rec_loss.item()
        rec_loss *= args.RM_coeff
            
            
        loss = criterion(output,y)
        probs_output = torch.exp(output) / (torch.exp(output).sum(1).view(-1,1))
        entropy = -(probs_output * torch.log(probs_output)).sum(1).mean()
        entropy_cost = args.entropy_coeff * F.relu(args.entropy_threshold - entropy)

        weight_divergence_cost = 0.0
        if(weight_divergence_coeff > 0.0):
            for own_p,main_p in zip(forward_model.parameters(),main_model.parameters()):
                weight_divergence_cost += weight_divergence_coeff * ((own_p - main_p)**2).sum()
        
        
        optimizer.zero_grad()
        (loss+rec_loss+entropy_cost + weight_divergence_cost).backward()
        optimizer.step()
#        print('in iter ',repr(iter))
        if iter == n_iterations:
           break
    print('trained for {} iterations'.format(iter))
    print('unique labels : ',unique_labels)
    print('total rec loss ', repr(total_rec_loss/n_iterations))



def federated_train(main_model,models,rec_models,optimizers,datasets,loss_eval_data,val_losses,val_accuracies,val_history,current_round,train_iters_per_round,weight_divergence_coeff,batch_size):
    n_models = len(models)
    for m in models:
        for (ownp,mainp) in zip(m.parameters(),main_model.parameters()):
            ownp.data.copy_(mainp.data)

    selected = random.sample(range(n_models),args.n_clients_per_round)
            
    def pretrain_validate(v_losses,v_accuracies,v_history):
        v_losses.append(val_losses[-1].copy())
        v_accuracies.append(val_accuracies[-1].copy())    

        acc,loss = validate(main_model,loss_eval_data)
        
        for model_idx in range(len(models)):
            v_losses[-1][model_idx] = loss
            v_accuracies[-1][model_idx] = acc
            v_history[model_idx].append([current_round,loss])
        
        # for model_idx in range(len(models)):
        #     valid_loader = torch.utils.data.DataLoader(datasets[model_idx],batch_size = args.val_batch_size,shuffle=False)        
        #     acc,loss = validate(models[model_idx],valid_loader,args.n_val_iters_per_round)
        #     v_losses[-1][model_idx] = loss
        #     v_accuracies[-1][model_idx] = acc
        #     v_history[model_idx].append([current_round,loss])

    t1 = time.time()
    if args.adaptive_hparams:
        pretrain_validate(val_losses,val_accuracies,val_history)
    adaptive_hparams_time = time.time() - t1
    model_train_times = []
    for model_idx in selected:
        print('training model {} for {} iters '.format(repr(model_idx),train_iters_per_round))
        loader = torch.utils.data.DataLoader(datasets[model_idx],batch_size = batch_size,shuffle=True,num_workers = args.num_loader_workers)
        print('using batch size ',repr(batch_size))
        opt = optimizers[model_idx]
        t1_model = time.time()
        if args.RM_mode:
            train_rec(models[model_idx],rec_models[model_idx],main_model,opt,loader,train_iters_per_round,weight_divergence_coeff = weight_divergence_coeff)
        else:
            train_vanilla(models[model_idx],opt,loader,main_model,train_iters_per_round,weight_divergence_coeff)
        model_train_times.append(time.time() - t1_model)
        
    selected_models= [models[model_idx] for model_idx in selected]
    selected_n_examples = [len(datasets[model_idx]) for model_idx in selected]

    print('averaging')

    trained_params = [list(m.parameters()) for m in selected_models]
    transposed_trained_params = [*zip(*trained_params)]

    
    norm = sum([len(D) for D in datasets])
    for param_set,main_p in zip(transposed_trained_params,main_model.parameters()):
        main_p.data.add_(sum([(m - main_p) * n_examples * 1.0 / norm  for m,n_examples in zip(param_set,selected_n_examples)]))
    return adaptive_hparams_time,model_train_times

def main():
    global args,device

    args = parser.parse_args()

    if args.use_cuda:
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
        


    kwargs = {'num_workers': 1, 'pin_memory': True}
    if args.task == 'mnist':

        train_dataset = torchvision.datasets.MNIST('./data', train=True, download=True,
                   transform=transforms.Compose([
                       transforms.ToTensor(),
                       transforms.Normalize((0.1307,), (0.3081,))
                   ]))
        single_class_datasets = None
        test_dataset = torchvision.datasets.MNIST('./data', train=False, transform=transforms.Compose([
                       transforms.ToTensor(),
                       transforms.Normalize((0.1307,), (0.3081,))
                   ]))

        test_loader = torch.utils.data.DataLoader(test_dataset,batch_size=1000, shuffle=False)
        

    elif args.task == 'cifar10':
        normalize = transforms.Normalize(mean=[x/255.0 for x in [125.3, 123.0, 113.9]],
                                   std=[x/255.0 for x in [63.0, 62.1, 66.7]])




        if args.augment:
            transform_train = transforms.Compose([
                    transforms.ToTensor(),
                    transforms.Lambda(lambda x: F.pad(x.unsqueeze(0),
                                                            (4,4,4,4),mode='reflect').squeeze()),
                transforms.ToPILImage(),
                transforms.RandomCrop(32),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                normalize,
                ])
        else:
            transform_train = transforms.Compose([
                transforms.ToTensor(),
                normalize,
                ])
        transform_test = transforms.Compose([
            transforms.ToTensor(),
            normalize
            ])

        train_dataset = torchvision.datasets.CIFAR10('./wr_data', train=True, download=True,
                             transform=transform_train)

        single_class_datasets = None
        test_loader = torch.utils.data.DataLoader(
            torchvision.datasets.CIFAR10('./wr_data', train=False, transform=transform_test),
            batch_size=100, shuffle=True, **kwargs)

    elif args.task == 'KWS':
        CLASSES = 'yes no up down left right on off stop go'.split()
        single_class_test_datasets = [single_class_speech_dataset(audio_load_transform(),cls,i,args.speech_commands_dataset,use_file = 'testing_list.txt',cache_in_memory = True)
                         for (i,cls) in enumerate(CLASSES)]
        single_class_datasets = [single_class_speech_dataset(audio_load_transform(),cls,i,args.speech_commands_dataset,exclude_files = ['testing_list.txt','validation_list.txt'],
                                              cache_in_memory = True) for (i,cls) in enumerate(CLASSES)]
        train_dataset = torch.utils.data.ConcatDataset(single_class_datasets)
        test_dataset = torch.utils.data.ConcatDataset(single_class_test_datasets)
        test_loader = torch.utils.data.DataLoader(test_dataset,batch_size=100, shuffle=False)

    else:
       raise RuntimeError('Unknown dataset {}. Dataset is first segment of network name'.format(dataset))

    print(args)
    global loaded_schedule
    with open(args.schedule_file, 'r') as stream:
        try:
            loaded_schedule = yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)


    def _make_model():
          if args.task == 'mnist':
              if args.RM_mode:
                  model = mnist_encoder(args.mnist_n_hidden)
              else:
                  model = mnist_model(args.mnist_n_hidden)
          elif args.task == 'cifar10':
              if args.RM_mode:
                  model = cifar10_encoder()
              else:
                  model = cifar10_model()
          elif args.task == 'KWS':
              if args.RM_mode:
                  model = speech_encoder()
              else:
                  model = speech_model()
          else:
              raise RuntimeError('unrecognized task name ' + repr(args.task))
          return model

    main_model = _make_model().to(device)
    client_models = [_make_model().to(device) for _ in range(10)]
    if args.RM_mode:
        if args.task == 'mnist':
            client_models_rec = [mnist_decoder(args.mnist_n_hidden).to(device) for _ in range(10)]
        elif args.task == 'cifar10':
            client_models_rec = [cifar10_decoder().to(device) for _ in range(10)]
        elif args.task == 'KWS':
            client_models_rec = [speech_decoder().to(device) for _ in range(10)]
    else:
        client_models_rec = None
        
    def _make_optimizer(params):
        return torch.optim.SGD(params,0.000,momentum = args.momentum, nesterov = args.nesterov, weight_decay  = 0.0)

    if args.RM_mode:
        optimizers = [_make_optimizer(list(m.parameters()) + list(m_rec.parameters())) for m,m_rec in zip(client_models,client_models_rec)]
    else:
        optimizers = [_make_optimizer(m.parameters()) for m in client_models]
        
    def _split_range(tot_len,splits):
        lengths =  [tot_len // splits]  * (splits - 1)
        lengths.append(tot_len - sum(lengths))
        return lengths

    if args.iid_dataset:
        datasets = torch.utils.data.random_split(train_dataset,_split_range(len(train_dataset),10))
    else:
        m = args.shards_per_client
        client_shards = np.ones((10,m),dtype='int32') * -1
        for assignment in range(m):
            while True:
                reserved_shards = []
                for i in range(10):
                    valid_shards = (set(np.arange(10)) -  set(client_shards[i,:assignment])) - set(reserved_shards)
                    if not(valid_shards): #empty valid_shards:
                        break
                    picked_shard = random.sample(list(valid_shards),1)[0]
                    client_shards[i,assignment] = picked_shard
                    reserved_shards.append(picked_shard)
                if np.all(client_shards[:,assignment] != -1):
                    break
                else:
                    print('trying again at assignment ',assignment)

        if single_class_datasets is None:
            single_class_datasets = [single_class_dataset(train_dataset,i,args.task + '_train') for i in range(10)]
        shards = [torch.utils.data.random_split(D,_split_range(len(D),args.shards_per_client))
                  for D in single_class_datasets]
        shard_allocation_position = np.zeros(10,dtype = 'int32')                     
        datasets = []
        for client_picks in client_shards:
            datasets.append(torch.utils.data.ConcatDataset([shards[i][shard_allocation_position[i]] for i in client_picks]))
            shard_allocation_position[client_picks] += 1
        
    criterion = nn.CrossEntropyLoss()

    train_loss_l = []
    test_loss_l = []
    val_loss_l = []    
    train_prec1_l = []
    test_prec1_l = []
    val_prec1_l = []    
    train_prec5_l = []
    test_prec5_l = []
    val_prec5_l = []    
    epoch_times_l = []
    adaptive_hparams_time_l = []
    model_train_times_l = []
    
    filename = args.task + '_' + repr(args.job_idx)

    val_losses = [np.ones(len(client_models))*-np.inf]
    val_accuracies = [np.ones(len(client_models))*-np.inf]

    hparam_per_round_history = []

    logprob_history = []
    val_history = [[] for _ in range(len(client_models))]
    hparams_distro_params_history = []
    sample_history = []

    loss_eval_data = None
    if args.adaptive_hparams:
        if args.task == 'KWS':
            n_iterations_points = [2,5,8,11,14,17,20,23,27,30]
        else:
            n_iterations_points = [2,5,10,20,30,40,50,60,70,80,90]
            
        hyperparams_points = [np.linspace(0.0005,0.05,20),n_iterations_points]
            
        hparam_distro = learnable_highd_gaussian(hyperparams_points,initial_precision = args.initial_precision,fixed_precision = False)

        hyper_optimizer = torch.optim.Adam(hparam_distro.parameters(),args.hyper_lr, betas = (0.7,0.7) )            

        eval_dataset = torch.utils.data.ConcatDataset([torch.utils.data.Subset(datasets[i],np.arange(args.val_batch_size * args.n_val_iters_per_round)) for i in range(10)])

        imgs = torch.stack([eval_dataset[i][0] for i in range(len(eval_dataset))])
        labels = torch.Tensor([eval_dataset[i][1] for i in range(len(eval_dataset))]).long()        
        loss_eval_data = (imgs,labels)
        print('evaluating loss using {} data points'.format(imgs.size(0)))
        
    for epoch in range(0, args.epochs):
        if args.refresh_optimizers:
            print('refreshing optimizers')
            if args.RM_mode:
                optimizers = [_make_optimizer(list(m.parameters()) + list(m_rec.parameters())) for m,m_rec in zip(client_models,client_models_rec)]
            else:
                optimizers = [_make_optimizer(m.parameters()) for m in client_models]
        b_size = args.train_batch_size
        lrate = get_schedule_val(loaded_schedule['lr'],epoch)
        train_iters_per_round = get_schedule_val(loaded_schedule['round_iters'],epoch)
        
        weight_divergence_coeff =  args.weight_divergence_coeff
        t1 = time.time()

        if args.adaptive_hparams:
            hparam,logprob,hparam_idx = hparam_distro()
            lrate,train_iters_per_round = hparam
            logprob_history.append(logprob)
            hparam_per_round_history.append(hparam_idx)
            sample_history.append(hparam)

        adaptive_hparams_time = time.time() - t1
        train_iters_per_round = int(train_iters_per_round + 0.5)
        if args.force_train_iters_per_round > 0:
            train_iters_per_round = args.force_train_iters_per_round
        
        for opt in optimizers:
            adjust_learning_rate(opt, lrate)

        adaptive_time,model_train_times  = federated_train(main_model,client_models,client_models_rec,optimizers,datasets,loss_eval_data,val_losses,val_accuracies,val_history,epoch,train_iters_per_round,weight_divergence_coeff,b_size)
        adaptive_hparams_time += adaptive_time
        model_train_times_l.append(model_train_times)
        
        t1_adaptive = time.time()
        if  args.adaptive_hparams:
            hyper_loss,imp = get_hyper_loss(hparam_distro,val_losses,max(0,epoch - args.baseline_cutoff_interval),hparam_per_round_history,logprob_history,
                                                   args.no_reinforce_baseline,args.windowed_updates)            
            if not(type(hyper_loss) == int):
                hyper_optimizer.zero_grad()
                (-hyper_loss).backward(retain_graph = True)
                hyper_optimizer.step()        

                print(' ##########hparams mean ', hparam_distro.mean.data.numpy() *  hparam_distro.vals_scale + hparam_distro.vals_center)
                print(' ##########hparams precision ', (hparam_distro.precision_component @ hparam_distro.precision_component.transpose(1,0)).data.numpy())
                
                hparams_distro_params_history.append([x.data.cpu().numpy().copy() for x in hparam_distro.parameters()])

        epoch_time = time.time() - t1
        adaptive_hparams_time += time.time() - t1_adaptive
        print('epoch {} main model train time : {} , adaptive hyper-params overhead :  {}'.format(epoch,repr(epoch_time),repr(adaptive_hparams_time)))
        epoch_times_l.append(epoch_time)
        adaptive_hparams_time_l.append(adaptive_hparams_time)
        
        prec1_train,prec5_train,loss_train = 0.0,0.0,0.0

        prec1_val,prec5_val,loss_val = 0.0,0.0,0.0
            
        prec1_test,prec5_test,loss_test = validate_fancy(test_loader, main_model, criterion, epoch,'test')

        test_loss_l.append(loss_test)
        train_loss_l.append(loss_train)            
        val_loss_l.append(loss_val)
        
        test_prec1_l.append(prec1_test)
        train_prec1_l.append(prec1_train)            
        val_prec1_l.append(prec1_val)
        
        test_prec5_l.append(prec5_test)
        train_prec5_l.append(prec5_train)
        val_prec5_l.append(prec5_val)
        
        print(main_model)
        # remember best prec@1 and save checkpoint
        filenames = [filename]
        #filenames+= [filename+'_atepoch_'+repr(epoch)]
        for f in filenames:
              save_checkpoint({
                  'test_loss' : test_loss_l,
                  'train_loss' : train_loss_l,
                  'val_loss' : val_loss_l,

                  'test_prec1' : test_prec1_l,
                  'train_prec1' : train_prec1_l,
                  'val_prec1' : val_prec1_l,            

                  'test_prec5' : test_prec5_l,
                  'train_prec5' : train_prec5_l,
                  'val_prec5' : train_prec5_l,            

                  'val_losses' : val_losses,
                  'val_accuracies' : val_accuracies,
                  'epoch_times' : epoch_times_l,
                  'model_train_times' : model_train_times_l,
                  'adaptive_hparams_time' : adaptive_hparams_time_l,
                  'hparams_distro_params_history' : hparams_distro_params_history,
                  'sample_history' : sample_history,
                  
                  
                  'model_name' : args.task,
                  'main_state_dict' : main_model.state_dict(),
                  'epoch': epoch + 1,
                  'args' : args
              }, filename = f)


              

def train_vanilla(model,optimizer,loader,main_model,n_iterations=-1,weight_divergence_coeff = 0.0):
    criterion = nn.CrossEntropyLoss().to(device)    
    model.train()
    iter = 0
    if(weight_divergence_coeff > 0.0):
        print('using weight divergence coeff ', repr(weight_divergence_coeff))

    unique_labels = np.empty(0)
    for (x,y) in loader:
        unique_labels = np.unique(np.concatenate((y.numpy(),unique_labels)))
        x,y = x.to(device),y.to(device)

        output = model(x)
        loss = criterion(output,y)

        weight_divergence_cost = 0.0
        if(weight_divergence_coeff > 0.0):
            for own_p,main_p in zip(model.parameters(),main_model.parameters()):
                weight_divergence_cost += weight_divergence_coeff * ((own_p - main_p)**2).sum()

        probs_output = torch.exp(output) / (torch.exp(output).sum(1).view(-1,1))
        entropy = -(probs_output * torch.log(probs_output)).sum(1).mean()
        entropy_cost = args.entropy_coeff * F.relu(args.entropy_threshold - entropy)


        optimizer.zero_grad()
        (loss+entropy_cost+weight_divergence_cost).backward()
        optimizer.step()
        iter += 1
        if iter == n_iterations:
           break 
    print('trained for {} iterations'.format(iter))
    print('unique labels : ',unique_labels)

def validate(model,loss_eval_data):
    criterion = nn.CrossEntropyLoss().to(device)
    
    n_errors = 0
    n_examples = 0
    model.eval()
    total_loss = 0.0
    batch_idx = 0
    x = loss_eval_data[0].to(device)
    y = loss_eval_data[1].to(device)    
    with torch.no_grad():
        if args.RM_mode:
            _,_,output = model(x)
        else:
            output = model(x) 

        loss  = criterion(output,y)
        total_loss += loss.item()
        error_pos = (output.argmax(1) != y)
        n_errors += error_pos.sum().item()
        n_examples += x.size(0)

        batch_idx += 1

    return n_errors * 100.0/n_examples,total_loss/batch_idx
#    return None,total_loss/batch_idx
    #return np.vstack(mistakes)

def validate_fancy(val_loader, model, criterion, epoch,pre_text):
    """Perform validation on the validation set"""
    batch_time = AverageMeter()
    losses = AverageMeter()
    top1 = AverageMeter()
    top5 = AverageMeter()

    # switch to evaluate mode
    model.eval()

    end = time.time()
    for i, (input, target) in enumerate(val_loader):
        target = target.to(device)
        input = input.to(device)

        # compute output
        with torch.no_grad():
            if args.RM_mode:
                _,_,output = model(input)
            else:
                output = model(input)
                
        loss = criterion(output, target)

        # measure accuracy and record loss
        prec1,prec5 = accuracy(output.data, target, topk=(1,5))
        losses.update(loss.data.item(), input.size(0))
        top1.update(prec1.item(), input.size(0))
        top5.update(prec5.item(), input.size(0))

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()


        
        if i % args.print_freq == 0:
            print('Test: [{0}/{1}]\t'
                      'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                      'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
                      'Prec@1 {top1.val:.3f} ({top1.avg:.3f})\t'
                      'Prec@5 {top5.val:.3f} ({top5.avg:.3f})'.format(
                       i, len(val_loader), batch_time=batch_time, loss=losses,
                       top1=top1, top5=top5))

    print(pre_text + ' * Prec@1 {top1.avg:.3f} Prec@5 {top5.avg:.3f} {losses.avg} '
              .format(top1=top1, top5=top5,losses = losses))

    return top1.avg,top5.avg,losses.avg


def save_checkpoint(state,  filename='checkpoint.pth.tar'):
    """Saves checkpoint to disk"""
    directory = "runs/"
    if not os.path.exists(directory):
        os.makedirs(directory)
    filename = directory + filename
    torch.save(state, filename)

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def get_schedule_val(schedule,query):
    val = list(schedule[-1].values())[0]
    for i,entry in enumerate(schedule):
        if query < list(entry)[0]:
            val = list(schedule[i-1].values())[0]
            break
    return val
        
def adjust_learning_rate(optimizer, lr):

    print('setting learning rate to ' + repr(lr))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr

def accuracy(output, target, topk=(1,)):
    """Computes the precision@k for the specified values of k"""
    maxk = max(topk)
    batch_size = target.size(0)

    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(target.view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].view(-1).float().sum(0)
        res.append(correct_k.mul_(100.0 / batch_size))
    return res



if __name__ == '__main__':
    main()
    

